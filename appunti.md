# Idee
* tasto per timbrare entrata o uscita
* memorizzazione orari settimana
* visualizzazione di quanti minuti sono arretrati giorno per giorno
* avviso se la settimana taglia il mese
* visualizzazione dell'orario ipotetico a cui bisognerebbe uscire in giornata per recuperare
  * se visualizzato di mattina, conta automaticamente 1 ora di pausa pranzo

# Note
* permettere giorno per giorno di entrare/uscire più volte?
* considerazione permessi?
* bottone entra/esci
  * entra => blu, esci => verde
  * cambia stato da uno all'altro a seconda che si sia dentro o fuori
  * floating in basso a destra
  * controlli per resettare lo stato del bottone
* riquadri orario giornata
  * idea: mostrano solo il tempo fatto, e sono espandibili tappandoci

# Idee per mockup 2
* le card dei giorni mostrano solo l'anteprima, sono espandibili per
  * visualizzare tutti gli orari
  * modificare orari
  * aggiungere permessi

# TODO refactor
* capire se si possono settare più valori a una data dayjs con una sola funzione

########## Carte

-----

# Testing
  * coprire di test il codice attuale come possibile

# Visualizzazione storico timbrature
  * trovare modo di salvare e ripescare le vecchie settimane di timbrature
  * trovare modo di scaricare resoconto
    * es. inviare email, vedi npm nodemailer e simili
    * es. Salvare file, vedi react-native-fetch-blob

# GUI permette di aggiungere entrate/uscite ulteriori
  * si può aggiungere una coppia di orari entrata/uscita
  * questa coppia si può rimuovere al bisogno

# Snackbar fastidiosa: non permette di vedere subito l'orario di uscita
  * valutare il da farsi

# Modifica manuale orari imposta le notifiche correttamente
  * e le modifica (es. inserisco fine pausa pranzo, la modifico => la notifica di fine giornata è reimpostata)

# widget android home
  * tasto entra/esci che cambia stato correttamente
  * dopo la pausa pranzo, mostra l'orario di uscita consigliato
  * durante la pausa pranzo, mostra l'orario di fine pausa
  * negli altri casi, non mostra nulla

# Il tasto entra/esci cambia stato correttamente
  * "Entra" verde o "Esci" rosso
  * ricorda da solo in quale stato è

# Menù di impostazioni attivo
  * modifica numero di ore giornaliero (default: 8)
  * durata pausa pranzo
  * modifica giorni in cui si vuole la notifica di quando si può uscire
  * impostazione della modalità di visualizzazione timepicker (spinner o default)
  * auto-reset settimana di Lunedì mattina (default: false)
  * impostazione orario entrata default
  * notifica n minuti prima di dover finire la pausa pranzo
    * durata default pausa pranzo

# Refactor
  * provare a usare tiny-emitter per i cambiamenti dei timestamp

* alert quando la settimana taglia il mese

# Bottoni entra/esci con grafica decente

# nuovo tab salva tempo lavorato diviso per carta
  * mostra le carte in progress da youtrack (c'è un'api?)
  * permette di avviare dei timer di lavoro su ogni carta e salvare poi il tempo lavorato
    * con pulsanti start/stop
  * permette di visualizzare il tempo lavorato totale giornaliero diviso per carta

# Icone app
  * icona per drawer
  * icone piccola e grande per notifiche

# Affidabilità db
  * spostare storage da AsyncStorage a sqlite o altro

# il TimePicker si apre su orario suggerito
  * orari suggeriti: 8:00, {4h30m dalla prima entrata}, {1h dalla prima uscita}, {4h dalla seconda entrata}