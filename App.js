import React from 'react';
import {AppState, SafeAreaView, Alert} from 'react-native';
import SnackBar from 'react-native-snackbar';
import Header from './src/components/Header';
import WeekTable from './src/components/WeekTable';
import WeekSummary from './src/components/WeekSummary';
import {Date, getDayString} from './src/util/datetime';
import WeekTableStorage from './src/WeekTable';
import Stamp from './src/model/Stamp';
import {
  scheduleLunchBreakEndNotification,
  scheduleDayEndNotification,
  cancelAllNotifications,
} from './src/util/notification';
import {getCumulatedMinutes} from './src/util/workingTime';

const DEBUG = false;

const styles = {
  container: {
    flex: 1,
  },
};

class App extends React.Component {
  weekTable = new WeekTableStorage();

  state = {
    date: DEBUG ? Date('01/16/2020') : Date(),
    dayTimecards: this.weekTable.dayTimecards,
    appState: AppState.currentState,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.refreshDaysData();

    AppState.addEventListener('change', appState => {
      const prevAppState = this.state.appState;
      const newPartialState = {appState};
      if (prevAppState.match(/inactive|background/) && appState === 'active') {
        newPartialState.date = Date();
      }
      this.setState(newPartialState);
    });
  }

  refreshDaysData() {
    this.weekTable.getDayTimecards().then(dayTimecards => {
      this.setState({dayTimecards});
    });
  }

  scheduleLunchBreakNotifications() {
    scheduleLunchBreakEndNotification();
    SnackBar.show({
      text: 'Notifiche fine pausa impostate',
      duration: 3000,
      action: {
        text: 'annulla',
        onPress: cancelAllNotifications,
      },
    });
  }

  async scheduleDayEndNotification() {
    const todayTimecard = await this.weekTable.getTodayTimecard();
    const cumulatedTimeAmount = getCumulatedMinutes(this.state.dayTimecards);

    const suggestedExitTime = todayTimecard.getSuggestedExitTime(
      cumulatedTimeAmount,
    );

    scheduleDayEndNotification(suggestedExitTime);

    SnackBar.show({
      text: 'Notifica fine giornata impostata',
      duration: 3000,
      action: {
        text: 'annulla',
        onPress: cancelAllNotifications,
      },
    });
  }

  onStampPress() {
    cancelAllNotifications();

    this.weekTable
      .stamp()
      .then(stampType => {
        if (stampType === Stamp.types.LUNCH_BREAK_START) {
          this.scheduleLunchBreakNotifications();
        } else if (stampType === Stamp.types.LUNCH_BREAK_END) {
          this.scheduleDayEndNotification();
        }
        this.refreshDaysData();
      })
      .catch(err => Alert.alert('Errore', err));
  }

  async onResetPress() {
    const undoReset = await this.weekTable.reset();

    this.refreshDaysData();

    const undoCallback = async () => {
      await undoReset();
      this.refreshDaysData();
    };

    SnackBar.show({
      text: 'Orari settimanali cancellati.',
      duration: 5000,
      action: {
        text: 'annulla',
        onPress: undoCallback,
      },
    });
  }

  onSettingsPress() {
    Alert.alert('Not implemented yet');
  }

  resetWeekTable() {
    this.weekTable.reset().then(this.refreshDaysData.bind(this));
  }

  onStampEdit(dayIndex, stampIndex, attribute, newTimestamp) {
    this.weekTable
      .update(dayIndex, stampIndex, attribute, newTimestamp)
      .then(() => this.refreshDaysData());
  }

  render() {
    const {date} = this.state;
    this.weekTable.setDate(date);

    return (
      <SafeAreaView style={styles.container}>
        <Header
          dayString={getDayString(date)}
          onSettingsPress={this.onSettingsPress.bind(this)}
        />
        <WeekTable
          dayTimecards={this.state.dayTimecards}
          onStampEdit={this.onStampEdit.bind(this)}
        />
        <WeekSummary
          date={this.state.date}
          dayTimecards={this.state.dayTimecards}
          onStampPress={this.onStampPress.bind(this)}
          onResetPress={this.onResetPress.bind(this)}
        />
      </SafeAreaView>
    );
  }
}

export default App;
