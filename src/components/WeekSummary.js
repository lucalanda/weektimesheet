import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import TimeAmount from './TimeAmount';
import PropTypes from 'prop-types';
import {isWeekend, PLACEHOLDER_TIME} from '../util/datetime';
import {getCumulatedMinutes} from '../util/workingTime';

const styles = {
  container: {
    paddingVertical: 12,
    paddingHorizontal: 24,
    flexDirection: 'row',
    backgroundColor: '#f5f5f5',
    elevation: 12,
  },
  summary: {
    flex: 0.55,
  },
  centered: {
    textAlign: 'center',
  },
  timeAmount: {
    fontSize: 16,
  },
  suggestedExitTime: {
    fontSize: 20,
  },
  actionButtons: {
    flex: 0.45,
    justifyContent: 'center',
  },
  button: {
    backgroundColor: '#c9c9c9',
    borderRadius: 3,
    padding: 8,
    marginHorizontal: 8,
  },
  buttonText: {
    color: 'black',
  },
  reset: {
    marginTop: 8,
  },
};

const AmountSummary = ({dayTimecards}) => {
  const workedDaysTimecards = dayTimecards.filter(dt => dt.isComplete());
  const minutesAmount = getCumulatedMinutes(workedDaysTimecards);

  return (
    <Text style={styles.centered}>
      {'Totale settimana: '}
      <TimeAmount amount={minutesAmount} style={styles.timeAmount} />
    </Text>
  );
};

const getSuggestedExitTime = (date, dayTimecards) => {
  const todayTimecard = dayTimecards[date.weekday()];

  if (isWeekend(date) || todayTimecard.hasExitTime()) {
    return PLACEHOLDER_TIME;
  }

  const cumulatedTimeAmount = getCumulatedMinutes(dayTimecards);

  return todayTimecard
    .getSuggestedExitTime(cumulatedTimeAmount)
    .format('HH:mm');
};

const WeekSummary = ({date, dayTimecards, onStampPress, onResetPress}) => (
  <View style={styles.container}>
    <View style={styles.summary}>
      <AmountSummary dayTimecards={dayTimecards} />
      <Text style={styles.centered}>Oggi dovresti uscire alle:</Text>
      <Text style={[styles.centered, styles.suggestedExitTime]}>
        {getSuggestedExitTime(date, dayTimecards)}
      </Text>
    </View>
    <View style={styles.actionButtons}>
      <TouchableOpacity style={styles.button} onPress={onStampPress}>
        <Text style={[styles.centered, styles.buttonText]}>Timbra</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.button, styles.reset]}
        onPress={onResetPress}>
        <Text style={[styles.centered, styles.buttonText]}>
          Reset settimana
        </Text>
      </TouchableOpacity>
    </View>
  </View>
);

WeekSummary.propTypes = {
  onStampPress: PropTypes.func.isRequired,
};

export default WeekSummary;
