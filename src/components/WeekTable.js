import React from 'react';
import {ScrollView, View} from 'react-native';
import DayTimecard from './DayTimecard';

const styles = {
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  placeholder: {
    height: 20,
  },
};

const Placeholder = () => <View style={styles.placeholder} />;

const WeekTable = ({dayTimecards, onStampEdit}) => {
  // TODO extract day names from dayjs
  const dayNames = ['Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì'];

  return (
    <ScrollView style={styles.container}>
      {dayTimecards.map((dayTimecard, i) => (
        <DayTimecard
          key={i}
          dayName={dayNames[i]}
          dayTimecard={dayTimecard}
          onStampEdit={(stampIndex, attribute, newTimestamp) =>
            onStampEdit(i, stampIndex, attribute, newTimestamp)
          }
        />
      ))}
      <Placeholder />
    </ScrollView>
  );
};

export default WeekTable;
