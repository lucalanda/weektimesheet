import React from 'react';
import {Text} from 'react-native';
import {getTimeAmountString} from '../util/datetime';
import PropTypes from 'prop-types';

const styles = {
  timeAmount: {
    fontWeight: '700',
  },
  positiveAmount: {
    color: 'green',
  },
  negativeAmount: {
    color: 'red',
  },
};

const TimeAmount = ({amount, style}) => {
  const finalStyle = [
    styles.timeAmount,
    amount >= 0 ? styles.positiveAmount : styles.negativeAmount,
    style,
  ];
  const timeAmountString = getTimeAmountString(amount);
  return <Text style={finalStyle}>{timeAmountString}</Text>;
};

TimeAmount.defaultProps = {
  workedDays: 1,
};

TimeAmount.propTypes = {
  workedDays: PropTypes.number.isRequired,
};

export default TimeAmount;
