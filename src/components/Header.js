import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import PropTypes from 'prop-types';

const styles = {
  container: {
    padding: 16,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    // start shadow props
    backgroundColor: 'white',
    shadowOffset: {height: 2},
    shadowColor: 'black',
    shadowOpacity: 0.6,
    elevation: 3,
    // end shadow props
  },
  dayString: {
    fontSize: 16,
  },
  image: {
    width: 20,
    height: 20,
  },
};

const gearIcon = require('../../res/img/gear_icon.png');

const Header = ({dayString, onSettingsPress}) => (
  <View style={styles.container}>
    <Text style={styles.dayString}>{dayString}</Text>
    <TouchableOpacity onPress={onSettingsPress}>
      <Image style={styles.image} source={gearIcon} />
    </TouchableOpacity>
  </View>
);

Header.propTypes = {
  dayString: PropTypes.string.isRequired,
  onSettingsPress: PropTypes.func.isRequired,
};

export default Header;
