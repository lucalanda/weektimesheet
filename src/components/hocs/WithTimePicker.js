import React from 'react';
import {View} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

const withTimePicker = WrappedComponent => {
  class WithTimePickerComponent extends React.Component {
    state = {
      show: false,
      defaultValue: new Date(),
      resolvePromise: () => {},
    };

    showTimePicker(timestamp) {
      const defaultValue =
        timestamp !== null ? new Date(timestamp) : new Date();

      return new Promise((resolvePromise, _reject) => {
        this.setState({defaultValue, show: true, resolvePromise});
      });
    }

    onChange({type, nativeEvent: {timestamp}}) {
      if (type === 'set') {
        this.state.resolvePromise(new Date(timestamp));
      }
      this.setState({show: false});
    }

    render() {
      const {show, defaultValue} = this.state;

      return (
        <View>
          <WrappedComponent
            {...this.props}
            showTimePicker={this.showTimePicker.bind(this)}
          />
          {show && (
            <DateTimePicker
              value={defaultValue}
              mode="time"
              display="spinner"
              is24Hour
              onChange={this.onChange.bind(this)}
            />
          )}
        </View>
      );
    }
  }

  return WithTimePickerComponent;
};

export default withTimePicker;
