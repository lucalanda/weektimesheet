import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import withTimePicker from './hocs/WithTimePicker';
import TimeAmount from './TimeAmount';
import {getCumulatedMinutes} from '../util/workingTime';
// import PropTypes from 'prop-types';

const styles = {
  container: {
    marginHorizontal: 16,
    paddingHorizontal: 8,
    marginTop: 8,
    paddingVertical: 8,
    // start ios shadow props
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    // end ios shadow props
    // start android shadow props
    elevation: 5,
    // end android shadow props
    backgroundColor: 'white',
    borderRadius: 10,
  },
  topSection: {
    paddingHorizontal: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  bottomSection: {
    marginTop: 2,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  dayName: {
    fontSize: 16,
  },
  stamp: {
    flexDirection: 'row',
    padding: 4,
  },
  stampContainer: {
    backgroundColor: '#aec5e8',
    height: 32,
    width: 64,
    borderRadius: 6,
    justifyContent: 'center',
  },
  stampTime: {
    textAlign: 'center',
    fontSize: 20,
  },
  stampTimeSeparator: {
    fontSize: 18,
  },
  timeAmount: {
    fontSize: 16,
  },
};

const Stamp = ({stamp, onEntryPress, onExitPress}) => (
  <View style={styles.stamp}>
    <TouchableOpacity onPress={onEntryPress}>
      <View style={styles.stampContainer}>
        <Text style={styles.stampTime}>{stamp.getEntryTimeString()}</Text>
      </View>
    </TouchableOpacity>
    <Text style={styles.stampTimeSeparator}> - </Text>
    <TouchableOpacity onPress={onExitPress}>
      <View style={styles.stampContainer}>
        <Text style={styles.stampTime}>{stamp.getExitTimeString()}</Text>
      </View>
    </TouchableOpacity>
  </View>
);

const DayTimecard = ({dayName, dayTimecard, onStampEdit, showTimePicker}) => {
  const editTime = (currentValue, editedStampIndex, editedStampAttribute) => {
    showTimePicker(currentValue).then(timestamp => {
      onStampEdit(editedStampIndex, editedStampAttribute, new Date(timestamp));
    });
  };

  const timeAmount = getCumulatedMinutes([dayTimecard]);
  const showAmount = dayTimecard.isComplete();

  return (
    <View style={styles.container}>
      <View style={styles.topSection}>
        <Text style={styles.dayName}>{dayName}</Text>
        {showAmount && (
          <TimeAmount style={styles.timeAmount} amount={timeAmount} />
        )}
      </View>
      <View style={styles.bottomSection}>
        {dayTimecard.getStamps().map((stamp, i) => (
          <Stamp
            key={i}
            stamp={stamp}
            onEntryPress={() => editTime(stamp.getEntryTime(), i, 'entryTime')}
            onExitPress={() => editTime(stamp.getExitTime(), i, 'exitTime')}
          />
        ))}
      </View>
    </View>
  );
};

export default withTimePicker(DayTimecard);
