import Stamp from './Stamp';
import { newFixedDate } from '../util/datetime';

class DayTimecard {
  constructor(stamps) {
    this.stamps = stamps;
  }

  stamp() {
    let set = false;
    let type = Stamp.types.DEFAULT;

    for (const stamp of this.stamps) {
      if (stamp.doStamp()) {
        set = true;
        break;
      }
    }
    if (set) {
      const isLunchBreakBegin =
        this.stamps[0].hasExitTime() && !this.stamps[1].hasEntryTime();

      if (isLunchBreakBegin) {
        type = Stamp.types.LUNCH_BREAK_START;
      }

      const isLunchBreakEnd =
        this.stamps[1].hasEntryTime() && !this.stamps[1].hasExitTime();

      if (isLunchBreakEnd) {
        type = Stamp.types.LUNCH_BREAK_END;
      }
    }

    return { set, type };
  }

  isComplete() {
    let complete = true;
    this.stamps.forEach(stamp => {
      complete = complete && stamp.hasEntryTime() && stamp.hasExitTime();
    });

    return complete;
  }

  getWorkingTimeMinutes() {
    return this.stamps.map(s => s.getWorkingTimeMinutes()).reduce((a, b) => a + b, 0);
  }

  getSuggestedExitTime(cumulatedAmountInMinutes = 0) {
    const entryTime = this.getEntryTime() || newFixedDate(8, 0);

    const launchBreakStart = this.stamps[0].getExitTime();
    const launchBreakEnd = this.stamps[1].getEntryTime();

    let exitTime = entryTime.add(8, 'hour');

    if (launchBreakStart !== null && launchBreakEnd !== null) {
      const launchBreakMinutes = launchBreakEnd.diff(
        launchBreakStart,
        'minute',
      );
      exitTime = exitTime.add(launchBreakMinutes, 'minute');
    } else {
      exitTime = exitTime.add(1, 'hour');
    }

    return exitTime.subtract(cumulatedAmountInMinutes, 'minute');
  }

  getEntryTime() {
    return this.stamps[0].getEntryTime();
  }

  hasExitTime() {
    return this.stamps[this.stamps.length - 1].hasExitTime();
  }

  getStamps() {
    return this.stamps;
  }

  static getPlaceholder() {
    return new DayTimecard([Stamp.getPlaceholder(), Stamp.getPlaceholder()]);
  }
}

export default DayTimecard;
