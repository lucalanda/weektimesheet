import {
  newFixedDateFromTimestamp,
  PLACEHOLDER_TIME,
  Date,
} from '../util/datetime';

class Stamp {
  static types = {
    LUNCH_BREAK_START: Symbol('lunchBreakStart'),
    LUNCH_BREAK_END: Symbol('lunchBreakEnd'),
    DEFAULT: Symbol('default'),
  };

  constructor(entryTime, exitTime) {
    this.entryTime =
      entryTime == null ? null : newFixedDateFromTimestamp(entryTime);
    this.exitTime =
      exitTime == null ? null : newFixedDateFromTimestamp(exitTime);
  }

  static getPlaceholder() {
    return new Stamp(null, null);
  }

  doStamp() {
    if (!this.hasEntryTime()) {
      this.entryTime = Date();
      return true;
    }

    if (!this.hasExitTime()) {
      this.exitTime = Date();
      return true;
    }

    return false;
  }

  getWorkingTimeMinutes() {
    if (this.entryTime === null || this.exitTime === null) {
      return 0;
    }

    return this.exitTime.diff(this.entryTime, 'minute');
  }

  getEntryTime() {
    return this.entryTime;
  }

  getExitTime() {
    return this.exitTime;
  }

  getEntryTimeString() {
    return this.hasEntryTime()
      ? this.entryTime.format('HH:mm')
      : PLACEHOLDER_TIME;
  }

  getExitTimeString() {
    return this.hasExitTime()
      ? this.exitTime.format('HH:mm')
      : PLACEHOLDER_TIME;
  }

  hasEntryTime() {
    return this.entryTime !== null;
  }

  hasExitTime() {
    return this.exitTime !== null;
  }
}

export default Stamp;
