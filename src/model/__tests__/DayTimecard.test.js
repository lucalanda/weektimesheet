import DayTimecard from '../DayTimecard';
import Stamp from '../Stamp';
import {aStamp} from '../../__tests__/__fixtures__/stampFixture';

describe('DayTimecard', () => {
  describe('.getWorkingTimeMinutes', () => {
    it('returns sum of its stamps working time minutes', () => {
      const stamps = [aStamp('08:30', '12:30'), aStamp('13:30', '17:00')];

      const dayTimecard = new DayTimecard(stamps);

      expect(dayTimecard.getWorkingTimeMinutes()).toBe(7 * 60 + 30);
    });
  });

  describe('.isComplete', () => {
    it('returns true when all timestamps are filled', () => {
      const dayTimecard1 = new DayTimecard([
        aStamp('08:30', '12:30'),
        aStamp('13:30', '17:00'),
      ]);

      const dayTimecard2 = new DayTimecard([
        aStamp('08:30', '12:30'),
        aStamp('13:30', null),
      ]);

      const dayTimecard3 = new DayTimecard([
        aStamp('08:30', '12:30'),
        aStamp(null, '17:00'),
      ]);

      expect(dayTimecard1.isComplete()).toBeTruthy();
      expect(dayTimecard2.isComplete()).toBeFalsy();
      expect(dayTimecard3.isComplete()).toBeFalsy();
    });
  });

  describe('.stamp', () => {
    it.each([
      [aStamp('08:30', '12:30'), aStamp('13:30', '17:00'), false],
      [aStamp('08:30', '12:30'), aStamp('13:30', null), true],
      [aStamp('08:30', '12:30'), aStamp(null, '17:00'), true],
    ])('returns set = true when a stamp was set', (firstStamp, secondStamp, expectedSet) => {
      const { set } = new DayTimecard([firstStamp, secondStamp]).stamp();

      expect(set).toBe(expectedSet);
    });

    it('stamps on its first void stamp', () => {
      const stamp1 = aStamp('08:30', '12:30');
      const stamp2 = aStamp(null, '17:00');
  
      new DayTimecard([stamp1, stamp2]).stamp();
  
      expect(stamp2.hasEntryTime()).toBeTruthy();
    });
  
    it.each([
      [aStamp(null, null), aStamp(null, null), Stamp.types.DEFAULT],
      [aStamp('08:30', null), aStamp(null, null), Stamp.types.LUNCH_BREAK_START],
      [aStamp('08:30', '12:00'), aStamp(null, null), Stamp.types.LUNCH_BREAK_END],
      [aStamp('08:30', '12:00'), aStamp('13:00', null), Stamp.types.DEFAULT],
    ])('returns correct stamp type', (currentStamp1, currentStamp2, expectedStampType) => {
      const dayTimecard = new DayTimecard([currentStamp1, currentStamp2]);
  
      const { type } = dayTimecard.stamp();
  
      expect(type).toBe(expectedStampType);
    });
  });

  describe.todo('.getSuggestedExitTime', () => {
    // TODO move method into a utility class??
  })

});
