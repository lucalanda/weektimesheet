import Stamp from '../Stamp';
import {Date} from '../../util/datetime';

const aTimestamp = '01/01/1970 08:35';
const aDate = Date(aTimestamp);

describe('Stamp', () => {
  describe('.constructor', () => {
    const inputExpectedTable = [
      [null, null, null, null],
      [null, aTimestamp, null, aDate],
      [aTimestamp, null, aDate, null],
    ];

    it.each(inputExpectedTable)(
      'initializes entryTime and exitTime correctly',
      (inputEntryTime, inputExitTime, expectedEntryTime, expectedExitTime) => {
        const stamp = new Stamp(inputEntryTime, inputExitTime);

        expect(stamp.getEntryTime()).toEqual(expectedEntryTime);
        expect(stamp.getExitTime()).toEqual(expectedExitTime);
      },
    );
  });

  describe('.doStamp', () => {
    it('sets entryTime property if null', () => {
      const stamp = new Stamp(null, aDate);
      stamp.doStamp();

      assertDateAndTimeMatch(stamp.getEntryTime(), Date());
    });

    it('sets exitTime property if null', () => {
      const stamp = new Stamp(aDate, null);
      stamp.doStamp();

      assertDateAndTimeMatch(stamp.getExitTime(), Date());
    });

    it('does not set any property if has entryTime and exitTime already', () => {
      const stamp = new Stamp(aDate, aDate);
      stamp.doStamp();

      assertDateAndTimeMatch(stamp.getEntryTime(), aDate);
      assertDateAndTimeMatch(stamp.getExitTime(), aDate);
    });

    it('returns true if a value was set', () => {
      const stamp = new Stamp(aDate, aDate);

      expect(stamp.doStamp()).toBe(false);
    });

    it('returns false if no value was set', () => {
      const stamp = new Stamp(aDate, null);

      expect(stamp.doStamp()).toBe(true);
    });
  });

  describe('.getWorkingTimeMinutes', () => {
    it('returns 0 if some timestamp is missing', () => {
      const stamp1 = new Stamp(null, aDate);
      const stamp2 = new Stamp(aDate, null);
      const stamp3 = new Stamp(null, null);

      expect(stamp1.getWorkingTimeMinutes()).toBe(0);
      expect(stamp2.getWorkingTimeMinutes()).toBe(0);
      expect(stamp3.getWorkingTimeMinutes()).toBe(0);
    });

    it.each([
      ['08:30', '08:30', 0],
      ['08:30', '12:29', 239],
      ['13:30', '15:15', 105],
    ])(
      'returns correct value if both timestamps are given',
      (entryTime, exitTime, expected) => {
        const stamp = new Stamp(
          Date(`01/01/1970 ${entryTime}`),
          Date(`01/01/1970 ${exitTime}`),
        );

        expect(stamp.getWorkingTimeMinutes()).toBe(expected);
      },
    );

    it('ignores seconds and milliseconds on count', () => {
      const stamp = new Stamp(
        Date('01/01/1970 08:30:59:999'),
        Date('01/01/1970 08:32:00:000'),
      );

      expect(stamp.getWorkingTimeMinutes()).toBe(2);
    });
  });
});

const assertDateAndTimeMatch = (date1, date2) => {
  let fixedDate1 = date1.second(0).millisecond(0);
  let fixedDate2 = date2.second(0).millisecond(0);

  expect(fixedDate1).toEqual(fixedDate2);
};
