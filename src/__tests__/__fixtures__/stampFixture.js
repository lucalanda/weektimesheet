import Stamp from '../../model/Stamp';

export const aStamp = (entryTime = '08:00', exitTime = '12:00') =>
    new Stamp(
        entryTime ? new Date('01/01/1970 ' + entryTime) : null,
        exitTime ? new Date('01/01/1970 ' + exitTime) : null
    );
