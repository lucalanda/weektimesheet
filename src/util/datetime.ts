import dayjs from 'dayjs';
import 'dayjs/locale/it';
import weekdayPlugin from 'dayjs/plugin/weekday';

dayjs.locale('it');
dayjs.extend(weekdayPlugin);

export const PLACEHOLDER_TIME = '--/--';

export const getDayString = (date: Date) => dayjs(date).format('dddd D MMMM');

export const Date = (timestamp: Date | string = null) => {
  if (timestamp === null) {
    return dayjs();
  }
  return dayjs(timestamp);
};

export const getTimeAmountString = (amountInMinutes: number) => {
  const sign = amountInMinutes >= 0 ? '+' : '-';

  const hours = Math.trunc(amountInMinutes / 60);
  const hoursAbs = Math.abs(hours);
  const minutes = Math.abs(amountInMinutes - hours * 60);
  return `${sign}${getTimeString(hoursAbs, minutes)}`;
};

export const newFixedDateFromTimestamp = timestamp => {
  let t = dayjs(timestamp);
  return newFixedDate(t.hour(), t.minute(), timestamp);
};

export const newFixedDate = (
  hours: number,
  minutes: number,
  timestamp = dayjs(),
) => {
  let t = dayjs(timestamp);
  t = t.hour(hours);
  t = t.minute(minutes);
  t = t.second(0);
  t = t.millisecond(0);

  return t;
};

export const isWeekend = date => [5, 6].includes(dayjs(date).weekday());

const getTimeString = (hours: number, minutes: number) => {
  return newFixedDate(hours, minutes).format('HH:mm');
};
