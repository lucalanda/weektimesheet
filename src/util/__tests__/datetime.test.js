import dayjs from 'dayjs';
import {
  getDayString,
  getTimeAmountString,
  newFixedDateFromTimestamp,
  newFixedDate,
  isWeekend,
} from '../datetime';

const date = new Date('Tue May 12 2020 15:16:13 GMT+0200');

describe('.getDayString', () => {
  it('prints day string correctly from a standard date', () => {
    expect(getDayString(date)).toEqual('martedì 12 maggio');
  });

  it('prints day string correctly from a dayjs date', () => {
    const dayjsDate = dayjs(date);

    expect(getDayString(dayjsDate)).toEqual('martedì 12 maggio');
  });
});

describe('.getTimeAmountString', () => {
  const inputExpectedTable = [[83, '+01:23'], [15, '+00:15'], [-354, '-05:54']];

  it.each(inputExpectedTable)('prints %i amount as %s', (input, expected) => {
    expect(getTimeAmountString(input)).toEqual(expected);
  });
});

describe('.newFixedDateFromTimestamp', () => {
  it('returns correct date object', () => {
    const timestamp = date.getTime();
    const fixedDate = newFixedDateFromTimestamp(timestamp);

    expect(fixedDate instanceof dayjs).toBeTruthy();
    expect(fixedDate.hour()).toEqual(15);
    expect(fixedDate.minute()).toEqual(16);
    expect(fixedDate.second()).toEqual(0);
    expect(fixedDate.millisecond()).toEqual(0);
  });
});

describe('.newFixedDate', () => {
  it('returns correct date object', () => {
    const fixedDate = newFixedDate(20, 33);

    expect(fixedDate instanceof dayjs).toBeTruthy();
    expect(fixedDate.hour()).toEqual(20);
    expect(fixedDate.minute()).toEqual(33);
    expect(fixedDate.second()).toEqual(0);
    expect(fixedDate.millisecond()).toEqual(0);
  });
});

describe('.isWeekend', () => {
  it.each`
    input                | expected
    ${'Mon May 11 2020'} | ${false}
    ${'Fri May 15 2020'} | ${false}
    ${'Sat May 16 2020'} | ${true}
    ${'Sun May 17 2020'} | ${true}
  `('returns $expectedVal for $input', ({input, expected}) => {
    expect(isWeekend(dayjs(input))).toBe(expected);
  });
});
