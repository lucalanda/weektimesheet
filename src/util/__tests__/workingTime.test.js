import { getCumulatedMinutes } from '../workingTime';
import { aStamp } from '../../__tests__/__fixtures__/stampFixture';
import DayTimecard from '../../model/DayTimecard';

describe('.getCumulatedMinutes', () => {

  let completeDayTimecard1;

  beforeAll(() => {
    completeDayTimecard1 = new DayTimecard([aStamp('08:30', '12:30'), aStamp('13:30', '17:25')])
  });

  it('returns 0 with no timecard', () => {
    expect(getCumulatedMinutes([])).toBe(0);
  });

  it('returns sum of day DayTimecards working time', () => {
    const dayTimecard2 = new DayTimecard([aStamp('09:30', '13:30'), aStamp('14:30', '18:45')]);

    expect(getCumulatedMinutes([completeDayTimecard1])).toBe(-5);
    expect(getCumulatedMinutes([dayTimecard2])).toBe(15);
    expect(getCumulatedMinutes([completeDayTimecard1, dayTimecard2])).toBe(10);
  });

  it('skips uncomplete DayTimecards', () => {
    const dayTimecards = [
      completeDayTimecard1,
      new DayTimecard([aStamp('08:00', '12:00'), aStamp('14:00', null)]),
      new DayTimecard([aStamp(null, null)]),
    ];

    expect(getCumulatedMinutes(dayTimecards)).toBe(-5);
  });
});
