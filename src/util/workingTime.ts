import DayTimecard from '../model/DayTimecard';

export const getCumulatedMinutes = (dayTimecards: [DayTimecard]) => {
  const completeTimecards = dayTimecards.filter(dt => dt.isComplete());

  const workedMinutes = completeTimecards
    .map(dt => dt.getWorkingTimeMinutes())
    .reduce((a, b) => a + b, 0);

  return workedMinutes - 8 * 60 * completeTimecards.length;
};
