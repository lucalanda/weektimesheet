import PushNotification from 'react-native-push-notification';
import {Date} from './datetime';

export const scheduleLunchBreakEndNotification = () => {
  const lunchBreakDurationMinutes = 60;
  const lunchBreakEndDate = Date().add(lunchBreakDurationMinutes, 'minute');

  PushNotification.localNotificationSchedule({
    title: 'Break',
    message: 'La pausa termina tra 5 minuti.',
    date: lunchBreakEndDate.subtract(5, 'minute').toDate(),
    vibration: 2500,
  });

  PushNotification.localNotificationSchedule({
    title: 'Break',
    message: 'La pausa è terminata.',
    date: lunchBreakEndDate.toDate(),
    vibration: 2500,
  });
};

export const scheduleDayEndNotification = (endTime: Date) => {
  PushNotification.localNotificationSchedule({
    title: 'Fine giornata',
    message: 'Ora puoi uscire!',
    date: endTime,
    vibration: 2500,
  });
};

export const cancelAllNotifications = () =>
  PushNotification.cancelAllLocalNotifications();
