import AsyncStorage from '@react-native-community/async-storage';
import DayTimecard from './model/DayTimecard';
import Stamp from './model/Stamp';
import {isWeekend, Date} from './util/datetime';

class WeekTable {
  constructor(date) {
    this.date = date;
    this.dayTimecards = this.getPlaceholderDayTimecards();
    this.reload();
  }

  reload() {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem('timetable', (error, timetable) => {
        if (error) {
          reject('some error occurred');
        }

        if (timetable === null) {
          this.dayTimecards = this.getPlaceholderDayTimecards();
        } else {
          const dayTimecardRecords = JSON.parse(timetable);

          const dayTimecards = [];
          dayTimecardRecords.forEach(record => {
            const stamps = record.stamps.map(
              ({entryTime, exitTime}) => new Stamp(entryTime, exitTime),
            );
            dayTimecards.push(new DayTimecard(stamps));
          });

          this.dayTimecards = dayTimecards;
        }

        resolve(this.dayTimecards);
      });
    });
  }

  update(dayIndex, stampIndex, attribute, newTimestamp) {
    // TODO refactoring
    this.dayTimecards[dayIndex].stamps[stampIndex][attribute] = Date(
      newTimestamp,
    );

    this.commitState();

    return Promise.resolve(true);
  }

  stamp() {
    return new Promise(async (resolve, reject) => {
      if (isWeekend(this.date)) {
        reject(`Oggi è ${this.date.format('dddd')}!`);
      }

      const todayTimecard = await this.getTodayTimecard();

      let {set, type} = todayTimecard.stamp();

      if (set) {
        this.commitState();
        resolve(type);
      }

      reject('Le timbrature di oggi sono complete.');
    });
  }

  async reset() {
    await AsyncStorage.removeItem('timetable');

    const dayTimecardsBackup = this.dayTimecards;
    const undoCallback = () => {
      this.dayTimecards = dayTimecardsBackup;
      return this.commitState();
    };

    return undoCallback;
  }

  getDayTimecards() {
    return this.reload().then(() => Promise.resolve(this.dayTimecards));
  }

  getTodayTimecard() {
    return this.reload().then(() => this.dayTimecards[this.date.weekday()]);
  }

  getPlaceholderDayTimecards() {
    const result = [];
    for (let i = 0; i < 5; i++) {
      result.push(DayTimecard.getPlaceholder());
    }

    return result;
  }

  commitState() {
    return AsyncStorage.setItem('timetable', JSON.stringify(this.dayTimecards));
  }

  setDate(date) {
    this.date = date;
  }
}

export default WeekTable;
